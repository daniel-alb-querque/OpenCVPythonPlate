import cv2
import numpy as np
import os
import DetectChars
import DetectPlates
import PossiblePlate
import sqlite3

SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

showSteps = False
printPlate = True

def main():
    capWebcam = cv2.VideoCapture(0)
    blnKNNTrainingSuccessful = DetectChars.loadKNNDataAndTrainKNN()
    while cv2.waitKey(1) != 27 and capWebcam.isOpened():            
        blnFrameReadSuccessfully, imgOriginal = capWebcam.read()           
        listOfPossiblePlates = DetectPlates.detectPlatesInScene(imgOriginal)          
        listOfPossiblePlates = DetectChars.detectCharsInPlates(listOfPossiblePlates) 
        if len(listOfPossiblePlates) != 0:
            listOfPossiblePlates.sort(key = lambda possiblePlate: len(possiblePlate.strChars), reverse = True)
            licPlate = listOfPossiblePlates[0]
            drawRedRectangleAroundPlate(imgOriginal, licPlate)
            if plateValidate(licPlate):
                saveEntry(licPlate)
        cv2.imshow("imgOriginalScene", imgOriginal)
    cv2.destroyAllWindows()                 
    return




###################################################################################################
def saveEntry(plate):
    try:
        printThePlate(plate)
        conn = sqlite3.connect("data.db")
        cursor = conn.cursor()
        cursor.execute("INSERT INTO FLUXO(PLACA) VALUES(?)", [(plate.strChars)])
        conn.commit()
    except Exception:
        pass



###################################################################################################
def printThePlate(plate):
    if printPlate:
        print "\nPLACA ===> " + plate.strChars + "\n"       
        print "----------------------------------------"

###################################################################################################
def representsInt(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


###################################################################################################
def plateValidate(plate):
    if(len(plate.strChars) != 7):
        return False;
    for i in range(0, 3):
        if(representsInt(plate.strChars[i])):
            return False
    for i in range(3, 7):
        if(not representsInt(plate.strChars[i])):
            return False
    return True;


###################################################################################################
def drawRedRectangleAroundPlate(imgOriginalScene, licPlate):
    p2fRectPoints = cv2.boxPoints(licPlate.rrLocationOfPlateInScene)           
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[0]), tuple(p2fRectPoints[1]), SCALAR_RED, 2)         
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[1]), tuple(p2fRectPoints[2]), SCALAR_RED, 2)
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[2]), tuple(p2fRectPoints[3]), SCALAR_RED, 2)
    cv2.line(imgOriginalScene, tuple(p2fRectPoints[3]), tuple(p2fRectPoints[0]), SCALAR_RED, 2)

###################################################################################################
if __name__ == "__main__":
    main()

